class Employee
{
    String firstName;
    String lastName;
    double salary;

    public Employee()
    {
        this.firstName = "";
        this.lastName = "";
        this.salary = 0.0;
    }
    public Employee(String firstName, String lastName, double salary)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        if(salary>0)
            this.salary = salary;
        else
            this.salary = 0.0;
    }
/*
    public void acceptData()
    {
        Console console = System.console();
        System.out.print("Enter first name: ");
        this.firstName = console.readLine();
        System.out.print("Enter last name: ");
        this.lastName = console.readLine();
        System.out.print("Enter first name: ");
        this.salary = Double.parseDouble(console.readLine());
    }
*/
    public String getFirstName()
    {
        return firstName;
    }
    public String getLastName()
    {
        return lastName;
    }
    public double getSalary()
    {
        return salary;
    }
    public double getSalaryRaise()
    {
        return salary+(salary*0.1);
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    public void setSalary(double salary)
    {
        this.salary = salary;
    }
/*
    public void printData()
    {
        System.out.println("Name = "+this.firstName+" "+this.lastName+"\nSalary = "+this.salary);
    }
*/
}

class EmployeeTest
{
    public static void main(String[] args)
    {
        Employee e1 = new Employee("Deepali","Marathe",50000);
        Employee e2 = new Employee("Surekha","Suryawanshi",-1500);
//        e1.printData();
//        e2.printData();
        e2.setSalary(100000);
        System.out.println("1.Full Name :"+e1.getFirstName()+" "+e1.getLastName());
        System.out.println("Salary before raise: "+e1.getSalary());
        System.out.println("Salary after raise: "+e1.getSalaryRaise());
        System.out.println("2.Full Name :"+e2.getFirstName()+" "+e2.getLastName());
        System.out.println("Salary before raise: "+e2.getSalary());
        System.out.println("Salary after raise: "+e2.getSalaryRaise());
    }
}