import java.io.Console;

class Population
{
    public static void main(String[] args)
    {
        Console console = System.console();
        System.out.print("Enter current population: ");
        int population = Integer.parseInt(console.readLine());
        System.out.print("Enter annual population growth rate: ");
        float rate = Float.parseFloat(console.readLine());
        int i;
        for(i=1; i<=5; i++)
        {
            population = population + (int)((rate*population/100));
            System.out.println("Estimated population after "+i+" year(s) is "+population);
        }
    }
}