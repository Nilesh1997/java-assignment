import java.io.Console;

class DrivingCost
{
    public static void main(String[] args)
    {
        Console console = System.console();
        System.out.println("Enter the given information");
        System.out.print("Total miles driven per day: ");
        float miles = Float.parseFloat(console.readLine());
        System.out.print("Cost per gallon of gasoline: ");
        float cost = Float.parseFloat(console.readLine());
        System.out.print("Average miles per day: ");
        float avgMiles = Float.parseFloat(console.readLine());
        System.out.print("Parking fees per day: ");
        float parking = Float.parseFloat(console.readLine());
        System.out.print("Tolls per day: ");
        float toll = Float.parseFloat(console.readLine());
        
        float total;
        total = ((miles/avgMiles) * cost) + parking + toll;

        System.out.println("Total driving cost = "+total);
    }
}
