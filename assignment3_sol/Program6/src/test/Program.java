package test;

import java.util.Scanner;

class Conversion{
	static double foreignExchange(double rupees) { //method to convert ₹ to $
		double result=0.0;
		result=(0.014)*rupees; // 1 Indian Rupee = $0.014
		return result;
		
	}
}
public class Program {
	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		
		double convertesValue=0.0,rupeesValue=0.0;
		
		System.out.print("Enter ₹");
		rupeesValue=sc.nextDouble();//taking rupees i/p
		
		convertesValue=Conversion.foreignExchange(rupeesValue);//converting using method
		
		System.out.println("₹"+rupeesValue+" = "+"$"+convertesValue);
		
	}

}
