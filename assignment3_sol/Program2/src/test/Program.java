package test;

import java.util.Scanner;

public class Program {
	static Scanner sc=new Scanner(System.in);
	static int []arr=new int[] {0,0,0,0,0,0,0,0,0,0};
	static int firstClassCount=0;
	static int economyClassCount=5;
	
	private static void Menu() {
		while(true) {
			System.out.println("0.Exit");
			System.out.println("1.First Class");
			System.out.println("2.Economy Class");
			System.out.println("");
			System.out.println("Enter Your Choice:");
			int ch=sc.nextInt();
			
			
			switch(ch) {
			case 1: Program.FirstClass();	
				break;
				
			case 2:Program.EconomyClass();

				break;
			case 0: 
				System.exit(0);
				
			default: 
				System.out.println("Attension it's wrong choice please Try again!!");
				System.out.println("");
				break;
			}
		}	
	}	


	private static void FirstClass() {
		System.out.println("");
		
		if(firstClassCount>4)
		{
			System.out.println("out of space placed in the economic-class section?");
			System.out.println("1.YES");
			System.out.println("2.NO");
			System.out.println("Enter your choice:");
			int ch=sc.nextInt();
			if (ch==1) {
				Program.EconomyClass();
			}
			else {
				System.out.println("Next flight leaves in 3 hours.");
				System.exit(0);
			}
			
		}
		else {
		
			arr[firstClassCount]=1;
			System.out.println("_____________________________________");
			System.out.println("	  Boarding Pass");
			System.out.println("	  ______________");
			System.out.println("");
			System.out.println("Seat No :"+(firstClassCount+1));
			System.out.println("section :First Class");
			System.out.println("");
			System.out.println("_____________________________________");
			
			firstClassCount++;
		}
		
		
		
	}


	private static void EconomyClass() {
		System.out.println("");
		if(economyClassCount==10) {
			System.out.println("Sorry we are out of space");
			System.out.println("Next flight leaves in 3 hours.");
			System.exit(0);
			
		}
		else if(economyClassCount>10)
		{
			
			System.out.println("out of space placed in the first-class section?");
			System.out.println("1.YES");
			System.out.println("2.NO");
			System.out.println("Enter your choice:");
			int ch=sc.nextInt();
			if (ch==1) {
				Program.FirstClass();
			}
			else {
				System.out.println("Next flight leaves in 3 hours.");
				System.exit(0);
			}
		}
		else {
			arr[economyClassCount]=1;
			
			System.out.println("_____________________________________");
			System.out.println("	  Boarding Pass");
			System.out.println("	  ______________");
			System.out.println("");
			System.out.println("Seat No :"+(economyClassCount+1));
			System.out.println("section :Economy Class");
			System.out.println("");
			System.out.println("_____________________________________");
			
			economyClassCount++;
			
		}	
	
	}

	public static void main(String[] args) {
	Program.Menu();
		
	}


}
