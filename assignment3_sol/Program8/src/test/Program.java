package test;

class Rational{
	private static int Numerator;
	private static int Denominator;
	/**/
	private static int numerator1;
	private static int denominator1;
	/*numerator1 and denominator1 for rational 1 */
	private static int numerator2;
	private static int denominator2;
	/*numerator2 and denominator2 for rational 2 */
	private static int[] arr= new int[2];
	/* to store the computed values and pass it using ref in reducedForm()*/
	private static int i;//use for loop
	
	public Rational() {
		Rational.numerator1=0;
		Rational.denominator1=1;
		Rational.numerator2=0;
		Rational.denominator2=1;
	}
	

	public static void setRational1(int numerator,int denominator) {
		Rational.numerator1=numerator;
		if(denominator==0) {
			System.out.println("In Rational1 denominator can't be 0");
			System.exit(0);
		}
		else {
			Rational.denominator1=denominator;
			
		}
		
	}
	
	public static void setRational2(int numerator,int denominator) {
		Rational.numerator2=numerator;
		if(denominator==0) {
			System.out.println("In Rational2 denominator can't be 0");
			System.exit(0);
		}
		else {
			Rational.denominator2=denominator;	
		}	
	}


	public static void addRational() {
		arr[0]=(denominator2*numerator1)+(denominator1*numerator2);
		arr[1]=(denominator1*denominator2);
		Rational.redusedForm(arr);//to convert into reduced form
		System.out.println("Addition of       "+numerator1+"/"+denominator1+" + "+
		numerator2+"/"+denominator2+" = "+Numerator+"/"+Denominator);
			
		}
	public static void substractRational() {
		arr[0]=(denominator2*numerator1)-(denominator1*numerator2);
		arr[1]=(denominator1*denominator2);
		Rational.redusedForm(arr);//to convert into reduced form
		System.out.println("Substraction of   "+numerator1+"/"+denominator1+" - "
		+numerator2+"/"+denominator2+" = "+Numerator+"/"+Denominator);
	}
	public static void multiplyRational() {
		arr[0]=(numerator1*numerator2);
		arr[1]=(denominator1*denominator2);
		Rational.redusedForm(arr);//to convert into reduced form
		System.out.println("Multiplication of "+numerator1+"/"+denominator1+" x "+
		numerator2+"/"+denominator2+" = "+Numerator+"/"+Denominator);

	}
	public static void divideRational() {
		arr[0]=(numerator1*denominator2);
		arr[1]=(denominator1*numerator2);
		Rational.redusedForm(arr);//to convert into reduced form
		System.out.println("Division of       "+numerator1+"/"+denominator1+" ÷ "
		+numerator2+"/"+denominator2+" = "+Numerator+"/"+Denominator);
	}
	public static double returnRational() {
		double rational=0.0;
		//TODO
		return rational;
		
	}
	public static void redusedForm(int[] arr) {
		Numerator=arr[0];//to inc readability
		Denominator=arr[1];//to inc readability
		if(Numerator>Denominator) {
			for(i=2;i<Numerator;i++) {
				if(Numerator%i ==0 & Denominator%i==0) {
					Numerator=Numerator/i;
					Denominator=Denominator/i;
				}
			}
		}
		else {
			for(i=2;i<Denominator;i++) {
				if(Numerator%i ==0 & Denominator%i==0) {
					Numerator=Numerator/i;
					Denominator=Denominator/i;
				}
			}	
		}
	}
}
public class Program {
	public static void main(String[] args) {
		Rational r = new Rational();
//		Rational.setRational1(1, 2);
//		Rational.setRational2(3, 4);
		Rational.setRational1(15, 15);
		Rational.setRational2(1, 1);
		Rational.addRational();
//		Rational.substractRational();
//		Rational.multiplyRational();
//		Rational.divideRational();
//	
	}

}
