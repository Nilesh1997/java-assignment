package test;

class SavingAccount {
	static double annulIntrestRate;
		private double savingBalance;
		
		
		public SavingAccount(double savingBalance) {
			this.savingBalance = savingBalance;
		}

		double calculateMonthlyInterest() {
			this.savingBalance+=(this.savingBalance*SavingAccount.annulIntrestRate)/(12*100);
			//as annul intrest is in % so devide by 100
			return savingBalance;
		}
		
		public static double getAnnulIntrestRate() {
			return annulIntrestRate;
		}

		public static void setAnnulIntrestRate(double annulIntrestRate) {
			SavingAccount.annulIntrestRate = annulIntrestRate;
		}

		public double getSavingBalance() {
			return savingBalance;
		}

		public void setSavingBalance(double savingBalance) {
			this.savingBalance = savingBalance;
		}

		static void modifyIntrestRate(double modifyIntrestRate) {
			SavingAccount.annulIntrestRate=modifyIntrestRate;
		}
		
	}

public class Program {

	public static void main(String[] args) {
		//for saver1
		SavingAccount saver1 = new SavingAccount(2000);//instance
		SavingAccount.setAnnulIntrestRate(4);//set intrest rate
		saver1.calculateMonthlyInterest();//calculate monthly intrest
		System.out.println("saver1 saving balance at 4% annul intrest rate:"+saver1.getSavingBalance());//print saving balance
		SavingAccount.modifyIntrestRate(5);//modify intrest
		saver1.calculateMonthlyInterest();//calculate monthly intrest
		System.out.println("saver1 saving balance at 5% annul intrest rate:"+saver1.getSavingBalance());//print saving balance
		
		
		//for saver2
		SavingAccount saver2 = new SavingAccount(3000);//instance
		SavingAccount.setAnnulIntrestRate(4);//set intrest rate
		saver2.calculateMonthlyInterest();//calculate monthly intrest
		System.out.println("saver2 saving balance at 4% annul intrest rate:"+saver2.getSavingBalance());//print saving balance
		SavingAccount.modifyIntrestRate(5);//modify intrest
		saver2.calculateMonthlyInterest();//calculate monthly intrest
		System.out.println("saver2 saving balance at 5% annul intrest rate:"+saver2.getSavingBalance());//print saving balance
		
	

	}

}
