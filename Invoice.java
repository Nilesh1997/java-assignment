import java.util.Scanner;

class Invoice
{
	String partNumber;
	String description;
	int quantity;
	double pricePerItem;
	public Invoice() {
		this("","",0,0.0);
	}
	public Invoice(String partNumber, String description, int quantity, double pricePerItem) {
		this.partNumber = partNumber;
		this.description = description;
		this.quantity = quantity;
		this.pricePerItem = pricePerItem;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		if(quantity>0)
			this.quantity = quantity;
		else
			this.quantity = 0;
	}
	public double getPricePerItem() {
		return pricePerItem;
	}
	public void setPricePerItem(double pricePerItem) {
		if(pricePerItem>0)
			this.pricePerItem = pricePerItem;
		else
			this.pricePerItem = 0.0;
	}
	public double getInvoiceAmount() {
		if(pricePerItem>0 & quantity>0)
			return pricePerItem*quantity;
		else
			return 0.0;
	}
}

class InvoiceTest{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		Invoice iv = new Invoice();
		System.out.print("Part Number: ");
		iv.setPartNumber(sc.nextLine());
		System.out.print("Description: ");
		iv.setDescription(sc.nextLine());
		System.out.print("Quantity: ");
		iv.setQuantity(sc.nextInt());
		System.out.print("Price (per Item): ");
		iv.setPricePerItem(sc.nextDouble());
		System.out.println("Total amount = "+iv.getInvoiceAmount());
	}
}