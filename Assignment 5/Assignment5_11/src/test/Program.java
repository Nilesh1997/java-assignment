package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Program {
	public static void main(String[] args) throws ParseException {
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the Date in format  : MM/dd/yyyy");
		String str=sc.next();
		SimpleDateFormat sd1=new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sd2=new SimpleDateFormat("MMMM dd,yyyy");
		Date date=sd1.parse(str);
		
		System.out.println(sd2.format(date));
		
		
		
		
	}
}		
		