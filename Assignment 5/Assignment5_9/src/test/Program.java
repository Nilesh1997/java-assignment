package test;

import java.util.Scanner;

class TestString implements Iterable<String>{
	String str;
	TestString(String string){
		this.str=string;
	}
	public TestString() {
		// TODO Auto-generated constructor stub
		this(null);
		}
	int IndexOf(char[] ch) {
		int counter=-1;
		for (char element : this.str.toCharArray()) {
			counter++;
			if(element==ch[0]) {
				return counter;
			}
			
		}
		
		return 0;
		
	}
	int lastIndexOf(char[] ch) {
		char[] arr=this.str.toCharArray();
		for(int index=this.str.length()-1;index>0;index--) {
			if(arr[index]==ch[0]) {
				return index;
				
			}
		}
		return 0;
	}
	
	
}


public class Program {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String");
		String inputString=sc.next();
		TestString ts=new TestString(inputString);
		System.out.println("Enter character which want to search   :");
		char[] ch=sc.next().toCharArray();
		System.out.println("Index     		:"+ts.IndexOf(ch));//travarse from start
		System.out.println("index From last	:"+ts.lastIndexOf(ch));//travarse from last
		
	}
	
}
