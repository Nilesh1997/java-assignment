package test;

import java.util.Scanner;

abstract class Employee {
    private String first_name;
    private String last_name;;
    private int ssn;
    double weeksal;
    static Scanner sc = new Scanner(System.in);

    void accept(){
        sc.nextLine();
        System.out.println("Enter First Name");
        this.first_name =  sc.nextLine();
        System.out.println("Enter Last Name");
        this.last_name =  sc.nextLine();
        System.out.println("Enter Your Social Security Number");
        this.ssn = sc.nextInt();
    }

    
    public double getWeeksal() {
        return this.weeksal;
    }

    public void setWeeksal(double weeksal) {
        this.weeksal = weeksal;
    }
    public void display(){
        System.out.println("First Name :    "+this.first_name);
        System.out.println("Last Name :     "+this.last_name);
        System.out.println("Social Security Number :     "+this.ssn);
        System.out.println("Weekly Salary :     "+this.weeksal);
    }
    abstract double earnings_calc();

}

class Salaried_Employee extends Employee{
    
    void accept(){
        super.accept();
        System.out.println("ENter Your Weekly Salary");
        this.weeksal = sc.nextInt();
        sc.nextLine();
    }
    public void display(){
        super.display();
        System.out.println("Weekly Salary :     "+weeksal);
    }


    @Override
    double earnings_calc(){
        return weeksal;
    }
}

class HourlyEmployee extends Employee{

    int hours_worked;
    int hourly_wage;

    void accept(){
        super.accept();
        System.out.println("Enter The Hours Worked");
        this.hours_worked = sc.nextInt();
        System.out.println("Enter the Hourly Waged");
        this.hourly_wage = sc.nextInt();
    }

    public void display(){
        super.display();
        System.out.println("Hours Worked :     "+hours_worked);
        System.out.println("Hourly Wage :       "+hourly_wage);
    }


    @Override
    double earnings_calc(){
        if(this.hours_worked < 40){
            return this.hourly_wage * this.hours_worked;
        }
        else if(this.hours_worked > 40){
            return  ((40 * this.hourly_wage) + ((this.hours_worked - 40) * (this.hourly_wage * 1.5)));
        }
        return this.weeksal;
    }
    
}

class Commisison_Employee extends Employee{
    int gross_sales;
    int commision_rate;

    void accept(){
        super.accept();
        System.out.println("Enter the Gross Sales");
        this.gross_sales = sc.nextInt();
        System.out.println("Enter Your Commission Rate");
        this.commision_rate = sc.nextInt();
    }
    
    public void display(){
        super.display();
        System.out.println("Gross Sales :     "+this.gross_sales);
        System.out.println("Commission Rate :     "+this.commision_rate);
    }

    @Override
    double earnings_calc(){
        return this.commision_rate * this.gross_sales;
    }
}

class BasePlus_CommisionEmployee extends Commisison_Employee{
    double bsalary;

    void accept(){
        super.accept();
        System.out.println("Enter Base Salary");
        this.bsalary = sc.nextInt();
    }

    public void display(){
        super.display();
        System.out.println("Base Salary :     "+this.bsalary);
    }
    

     @Override
     double earnings_calc(){
         return ((this.commision_rate * this.gross_sales)+this.bsalary);
     }
}

public class Program{
    static Scanner sc1 = new Scanner(System.in);
    static public int menu(){
        System.out.println("Enter from the below show choices");
        System.out.println("1.Salaried Employees");
        System.out.println("2.Hourly Employee");
        System.out.println("3.Commission Employee");
        System.out.println("4.BasePlus Commissioned Employee");
        int ch = sc1.nextInt();
        return ch;
    }
    
    public static void main(String[] args) {
        Employee emp = null;
        int ch = menu();
        while(ch !=0){
            switch ((ch)) {
                case 1: emp = new Salaried_Employee();	//upcasting
                        break;
                case 2: emp = new HourlyEmployee();
                        break;
                case 3: emp = new Commisison_Employee();
                        break;
                case 4: emp = new BasePlus_CommisionEmployee();
                        break;
                case 0 : System.exit(0);
                default:System.out.println("Invalid Choice");
                        break;
            }
            if(emp != null){
                emp.accept();
                emp.setWeeksal(emp.earnings_calc()); //*
                emp.display();
            }
            ch = menu();
        }
        
    }
}
