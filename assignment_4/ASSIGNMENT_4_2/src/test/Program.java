package test;

import java.util.Scanner;

class Shape{
	public double area;
	public double perimeter;
	
	public void Area( ) {
		this.area = 0;
	}
	public double getArea() {
		return area;
	}
	public void Perimeter() {
		this.perimeter = 0;
	}
	public double getPerimeter() {
		return perimeter;
	}
}
class Rect extends Shape{
	private float length;
	private float breadth;
	public void setLength(float length) {
		this.length = length;
	}
	public void setBreadth(float breadth) {
		this.breadth = breadth;
	}
	public void Area( ) {
		this.area = this.length * this.breadth;
	}
	public void Perimeter() {
		this.perimeter = 2 * (this.length + this.breadth);
	}
}
class Circle extends Shape{
	private float radius;
	public void setRadius(float radius) {
		this.radius = radius;
	}
	public void Area( ) {
		this.area = (float) (Math.PI * Math.pow(this.radius, 2));
	}
	public void Perimeter( ) {
		this.perimeter= (float) (2*Math.PI*this.radius );
	}
	
}
class Triangle extends Shape{
	private float L;
	private float B;
	private float H;
	public void setlength(float L) {
		this.L = L;
	}
	public void setBase(float B) {
		this.B = B;
	}
	public void setHeight(float H) {
		this.H = H;
	}
	
	public void Area( ) {
		this.area = 0.5 * (this.B * this.H);
	}
	public void Perimeter( ) {
		this.perimeter = this.B + this.H + this.L;
	}
	
}
class ShapeFactory{
	public static Shape getInstance( int choice ) {
		Shape shape = null;
		switch( choice ) {
		case 1:
			shape = new Rect();	//Upcasting
			break;
		case 2:
			shape = new Circle();
			break;
		case 3:
			shape = new Triangle();
			break;
		}
		return shape;
	}
}
public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptRecord(Shape shape) {
		if( shape instanceof Rect )
		{
			Rect rect = (Rect) shape; //Downcasting
			System.out.print("Length	:	");
			rect.setLength(sc.nextFloat());
			System.out.print("Breadth	:	");
			rect.setBreadth(sc.nextFloat());
		}
		else if(shape instanceof Circle)
		{
			Circle c = (Circle) shape;	//Downcasting
			System.out.print("Radius	:	");
			c.setRadius(sc.nextFloat());
		}
		else
		{
			Triangle t = (Triangle) shape;	//Downcasting
			System.out.print("Length	:	");
			t.setlength(sc.nextFloat());
			System.out.print("Base	:	");
			t.setBase(sc.nextFloat());
			System.out.print("Height	:	");
			t.setHeight(sc.nextFloat());
		}
	}
	private static void printRecord(Shape shape) {
		System.out.println("Area	:	"+shape.getArea());
		System.out.println("Perimeter	:	"+shape.getPerimeter());
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Rectangle");
		System.out.println("2.Circle");
		System.out.println("3.Triangle");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		while( ( choice = Program.menuList( ) ) != 0 ) {
			Shape shape = ShapeFactory.getInstance(choice);
			if( shape != null ) {
				Program.acceptRecord( shape );
				shape.Area( );//DMD
				shape.Perimeter();
				Program.printRecord( shape );
			}
		}
	}
}
