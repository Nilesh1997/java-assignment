package test;

import java.time.LocalDate;
import java.util.Scanner;


class Address {
	private String cityName;	
	private String stateName;		
	private int pincode;
	public Address() {
		this.cityName = new String();
		this.stateName = new String();
		this.pincode = 0; 
	}
	public Address(String cityName, String stateName, int pincode) {
		this.cityName = cityName;
		this.stateName = stateName;
		this.pincode = pincode;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	
}

class Date {
	private int day;
	private int month;
	private int year;
	
	public Date() {
		LocalDate ld = LocalDate.now();
		this.day= ld.getDayOfMonth();
		this.month= ld.getMonthValue();
		this.year= ld.getYear();
	}
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	

}

class Person {
	private String name;	
	private Date birthDate;	
	private Address currentAddress;	
	
	public Person() {
		this.name = new String(); 
		this.birthDate = new Date();
		this.currentAddress = new Address();
	}
	public Person(String name, Date birthDate, Address currentAddress) {
		this.name = name;
		this.birthDate = birthDate;
		this.currentAddress = currentAddress;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Address getCurrentAddress() {
		return currentAddress;
	}
	public void setCurrentAddress(Address currentAddress) {
		this.currentAddress = currentAddress;
	}
}

public class Program {
	static Scanner sc = new Scanner(System.in);

	private static void acceptRecord(Date date) {
		if (date != null) {
			System.out.println("Enter Day: ");
			date.setDay(sc.nextInt());
			System.out.println("Enter Month: ");
			date.setMonth(sc.nextInt());
			System.out.println("Enter Year: ");
			date.setYear(sc.nextInt());
			
		}
	}
	private static void acceptRecord(Address address) {
		if (address!=null) {
			System.out.print("Enter Cityname	:	");
			address.setCityName(sc.next());
			System.out.print("Enter Statename	:	");
			address.setStateName(sc.next());
			System.out.print("Enter Pincode	:	");
			address.setPincode(sc.nextInt());
		}
	}
	private static void acceptRecord(Person person) {
		
		if (person != null) {
			System.out.println("Enter Name: ");
			person.setName(sc.next());
			System.out.println("Enter Birthdate: ");
			acceptRecord(person.getBirthDate());
			System.out.println("Enter Address: ");
			acceptRecord(person.getCurrentAddress());

		}
		
	}
	private static void printRecord(Date date) {
		
		System.out.println("Date: "+date.getDay()+"/"+date.getMonth()+"/"+date.getYear());
	}
	private static void printRecord(Address address) {
		System.out.println("Address:"+address.getCityName()+","+address.getStateName()+","+address.getPincode());
	}
	private static void printRecord(Person person) {
		System.out.println("Name: "+person.getName());
		System.out.println("Birthdate: ");
		printRecord(person.getBirthDate());
		System.out.println("Address: ");
		printRecord(person.getCurrentAddress());
	}
	public static int menuList() {
		System.out.println("1.Date");
		System.out.println("2.Address");
		System.out.println("3.Person");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}

	public static void main(String[] args) {
		int choice;
		while ((choice = Program.menuList()) != 0) {
			switch (choice) {
			case 1:
				Date date  = new Date();
				Program.acceptRecord( date );
				Program.printRecord( date );
				break;
			case 2:
				Address address = new Address();
				Program.acceptRecord( address );
				Program.printRecord( address );
				break;
			case 3:
				Person person = new Person();
				Program.acceptRecord( person );
				Program.printRecord( person );
				break;
			}
		}
	}
}
