import java.io.Console;

class Calculator
{
    public static void main(String[] args)
    {
        Console console = System.console();
        System.out.print("Enter the first number: ");
        int num1 = Integer.parseInt(console.readLine());

        System.out.print("Enter the second number: ");
        int num2 = Integer.parseInt(console.readLine());

        System.out.println("Enter the operation\n 1.Addition \n 2.Subtraction \n 3.Multiplication \n 4.Division");
        int op = Integer.parseInt(console.readLine());

        double result=0;

        switch(op)
        {
            case 1:
                result = num1+num2;
                break;
            case 2:
                result = num1-num2;
                break;
            case 3:
                result = num1*num2;
                break;
            case 4:
                if(num2 != 0)
                    result = num1/num2;
                else
                    System.out.println("For division, denomenator must not be zero");
                break;
            default:
                System.out.println("Incorrect operator");
                return;
        }

        System.out.println("result = "+result);
    }
}