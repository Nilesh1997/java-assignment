import java.io.Console;

class BmiCalculator
{
    public static void main(String[] args)
    {
        Console console = System.console();
        System.out.print("Enter weight in kilogram: ");
        float weight = Float.parseFloat(console.readLine());

        System.out.print("Enter height in meters: ");
        float height = Float.parseFloat(console.readLine());

        float bmi;
        bmi = weight / (height * height);
        System.out.println("BMI = "+bmi);

        if(bmi<18.5)
            System.out.println("Underweight");
        else if(18.5<bmi & bmi<24.9)
            System.out.println("Normal");
        else if(24.9<bmi & bmi<29.9)
            System.out.println("Overweight");
        else
            System.out.println("Obese");

    }
}