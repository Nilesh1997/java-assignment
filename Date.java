class Date
{
    int day, month, year;
    public Date(  )
    {
        this(0,0,0);    //Constructor Chaining
    }
    public Date( int day, int month, int year )
    {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public int getDay()
    {
        return day;
    }
    public int getMonth()
    {
        return month;
    }
    public int getYear()
    {
        return year;
    }

    public void setDay(int day)
    {
        this.day = day;
    }
    public void setMonth(int month)
    {
        this.month = month;
    }
    public void setYear(int year)
    {
        this.year = year;
    }

    public void displayDate()
    {
        System.out.println(this.day+" / "+this.month+" / "+this.year);
    }

}


class DateTest
{
    public static void main(String[] args)
    {
        Date d1 = new Date(22,11,1991);
        Date d2 = new Date(30,11,1968);

        d1.displayDate();
        d2.displayDate();
        
        d2.setYear(1970);
        d2.displayDate();

        System.out.println("Date of d1 : "+d1.getDay()+"/"+d1.getMonth()+"/"+d1.getYear());
        System.out.println("Date of d2 : "+d2.getDay()+"/"+d2.getMonth()+"/"+d2.getYear());

    }
}